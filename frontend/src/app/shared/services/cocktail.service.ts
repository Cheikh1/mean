import { Injectable } from '@angular/core';
import { Cocktail } from '../models/cocktail.model';
import { BehaviorSubject, Observable } from 'rxjs';
import { Ingredient } from '../models/ingredient.model';
import { HttpClient } from '@angular/common/http';
import { filter, map } from 'rxjs/operators';

@Injectable()
export class CocktailService {
  public cocktails: BehaviorSubject<Cocktail[]> = new BehaviorSubject([
    new Cocktail(
      'Bissap',
      'https://makemeacocktail.com/images/cocktails/8836/400_300_Jus-de-bissap.png',
      'Une boisson traditionnelle au Sénégal. Traditionnellement, elle est faite avec des fleurs d\'hibiscus au lieu du jus de canneberge. On laisse la fleur macérer pendant 10 minutes dans de l\'eau chaude..',
      [
        new Ingredient('hibiscus', 1),
        new Ingredient('feuilles de menthe', 2),
        new Ingredient('sucre', 3)
      ]),
    new Cocktail(
      'Ditakh',
      'https://scontent-iad3-1.cdninstagram.com/v/t51.2885-15/sh0.08/e35/s640x640/56929168_2281841315425571_1161789773094998139_n.jpg?_nc_ht=scontent-iad3-1.cdninstagram.com&_nc_cat=111&_nc_ohc=_jL_rIFrxKIAX-K1wEA&oh=595d2ab12fd988545ccd68ff29a275cc&oe=5ED0DB03',
      'Le ditakh est un fruit abondamment consommé au Sénégal. On le retrouve sur le marché sous forme de jus de fruits, de sirop, de marmelade ou encore de sorbet. Il est issu du detarium senegalensis, et est très apprécié pour sa chair fibreuse et acidulé qui entoure son noyau. Il peut être consommé cru, après avoir été épluché..',
      [
        new Ingredient('ditakh', 1),
        new Ingredient('sucre', 2),
        new Ingredient('fleur d\'oranger', 1)
      ]),
    new Cocktail(
      'Bouye',
      'https://ileauxepices.com/blog/wp-content/uploads/2017/09/bouye-jus-de-baobab-s%C3%A9n%C3%A9galais.jpg',
      'Cette boisson typique du Sénégal s’appelle bouye en Wolof, ou bizn baak ou bakou en langue Sérère. Certains l’appellent vin de singe. Avec le jus de bissap et le jus de gingembre, c’est l’une des boissons les plus populaires au Sénégal.',
      [
        new Ingredient('pain de singe', 1),
        new Ingredient('lait', 1),
        new Ingredient('sure', 2),
      ]),
  ]);

  /*constructor(private http: HttpClient) {
    this.cocktailsInit();
  }

  cocktailsInit(): void {
    this.http
      .get('')
      .subscribe((cocktails: Cocktail[]) => {
        this.cocktails.next(cocktails);
      });
  }*/

  getCocktail(index: number): Observable<Cocktail> {
    return this.cocktails.pipe(
      filter((cocktails) => cocktails != null),
      map((cocktails) => cocktails[index])
    );
  }

  addCocktail(cocktail: Cocktail): void {
    const cocktails = this.cocktails.value.slice();
    cocktails.push(
      new Cocktail(
        cocktail.name,
        cocktail.img,
        cocktail.desc,
        cocktail.ingredients.map(
          (ingredient) => new Ingredient(ingredient.name, ingredient.quantity)
        )
      )
    );
    this.cocktails.next(cocktails);
  }

  editCocktail(editCocktail: Cocktail): void {
    const cocktails = this.cocktails.value.slice();
    const index = cocktails.map((c) => c.name).indexOf(editCocktail.name);
    cocktails[index] = editCocktail;
    this.cocktails.next(cocktails);
    //this.save();
  }

  /*deleteCocktail(Cocktail: Cocktail): void {
    const cocktails = this.cocktails.value.slice();
    const index = cocktails.map((c) => c.name).indexOf(Cocktail.name);
    cocktails[index] = Cocktail;
    this.cocktails.next(cocktails);

  }



  save(): void {
    this.http
      .put(
        '',
        this.cocktails.value
      )
      .subscribe((res) => console.log(res));
  }*/
}
