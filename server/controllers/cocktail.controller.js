const Cocktail = require('../models/cocktail');

const cocktailCtr = { };

cocktailCtr.getCocktails = async (req, res) => {
    const cocktails = await Cocktail.find();
    res.json(cocktails);
};

cocktailCtr.createCocktail = async (req, res) => {
    const cocktail = new Cocktail(req.body);
    await cocktail.save();
    res.json({status: 'Cocktail Saved' });
};


cocktailCtr.getCocktail = async (req, res) => {
    const cocktail = await Cocktail.findById(req.params.id);
    res.json(cocktail);
}

cocktailCtr.editCocktail = async (req, res) => {
    const { id } = req.params;
    const cocktail = {
        name: req.body.name,
        img: req.body.img,
        desc: req.body.desc,
        ingredients: req.body.ingredients
    };
    Cocktail.findByIdAndUpdate(id, { $set: cocktail}, { new: true});
    res.json({status: 'Cocktail Updated'});
}

cocktailCtr.deleteCocktail = async (req, res) => {
    await Cocktail.findByIdAndRemove(req.params.id);
    res.json({status: 'Cocktail Deleted'});

}

module.exports = cocktailCtr;
