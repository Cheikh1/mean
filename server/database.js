const mongoose = require('mongoose');

mongoose.connect('mongodb+srv://store:123123123@cluster0-fe8mk.mongodb.net/store?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true
}, function (error) {
    if (error) {
        console.log(error);
    } else {
        console.log('Connexion ok');
    }
});

module.exports = mongoose;
