const mongoose = require('mongoose');
const { Schema } = mongoose;

const CocktailSchema = new Schema ({
    name: {
        type: String
    },
    img: {
        type: String
    },
    desc: {
        type: String
    },
    ingredients: [{
        type: String
    }]
});

module.exports = mongoose.model('Cocktail', CocktailSchema);


