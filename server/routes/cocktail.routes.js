const express = require('express');
const router = express.Router();

const cocktail = require('../controllers/cocktail.controller');

router.get('/', cocktail.getCocktails);
router.post('/', cocktail.createCocktail);
router.get('/:id', cocktail.getCocktail);
router.put('/:id', cocktail.editCocktail);
router.delete('/:id', cocktail.deleteCocktail);

module.exports = router;
